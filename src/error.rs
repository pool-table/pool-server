#[derive(Debug)]
pub enum Error {
    WebSocket(tungstenite::Error),
    Serde(serde_json::Error),

    MissingClient,
}
