use crate::client::Client;
use serde::{Deserialize, Serialize};
use tokio::net::TcpStream;
use tokio_tungstenite::WebSocketStream;

use crate::{client::ClientType, Error};
use futures_util::{stream::SplitSink, SinkExt};
use log::*;
use serde_json::{Map, Value};
use tungstenite::Message as TungsteniteMessage;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct SessionInformation {
    pub session_uuid: String,
    pub client_uuid: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Message {
    #[serde(flatten)]
    pub session: SessionInformation,
    pub content: MessageContent,
}

impl Message {
    pub async fn send(&self, client: &Client) -> Result<(), Error> {
        let mut channel = client.connection.lock().await;
        let serialized = serde_json::to_string(self).map_err(Error::Serde)?;
        send_str(&serialized, &mut channel).await
    }

    pub async fn send_to_all<'a, T>(&self, clients: T) -> Result<(), Error>
    where
        T: Iterator<Item = &'a Client>,
    {
        let serialized = serde_json::to_string(self).map_err(Error::Serde)?;
        for client in clients {
            let mut channel = client.connection.lock().await;
            match send_str(&serialized, &mut channel).await {
                Err(e) => error!("Error sending {:?}", e),
                _ => {}
            }
        }
        Ok(())
    }
}

pub async fn send_str(
    message: &str,
    channel: &mut SplitSink<WebSocketStream<TcpStream>, TungsteniteMessage>,
) -> Result<(), Error> {
    channel
        .send(TungsteniteMessage::text(message))
        .await
        .map_err(Error::WebSocket)?;

    Ok(())
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(tag = "t", content = "c")]
pub enum MessageContent {
    Register,
    SubmitDescription {
        description: Map<String, Value>,
        client_type: ClientType,
    },
    IceCandidate {
        candidate: Map<String, Value>,
    },
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_message_serialize() {
        
    }
}
