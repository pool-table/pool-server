use tokio::net::TcpStream;
use tokio_tungstenite::WebSocketStream;
use tungstenite::Message as TungsteniteMessage;
use futures_util::stream::SplitSink;
use futures_util::lock::Mutex;
use std::sync::Arc;
use serde::{Deserialize, Serialize};

type SendConnection = Arc<Mutex<SplitSink<WebSocketStream<TcpStream>, TungsteniteMessage>>>;

#[derive(Debug)]
pub struct Client {
    pub uuid: String,
    pub connection: SendConnection
}

impl Client {
    pub fn new(uuid: String, connection: SendConnection) -> Self {
        Self {
            uuid,
            connection,
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone, Copy)]
pub enum ClientType {
    Game,
    Cue,
}
