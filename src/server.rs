use dashmap::DashMap;
use futures_util::lock::Mutex;
use futures_util::stream::SplitSink;
use futures_util::{StreamExt};
use log::*;
use std::net::SocketAddr;
use std::sync::Arc;
use tokio::net::{TcpListener, TcpStream};
use tokio_tungstenite::{accept_async, WebSocketStream};
use tungstenite::{Error as TungsteniteError, Message as TungsteniteMessage};

use crate::{Client, Error, Message, MessageContent};

struct ServerState {
    registration_clients: DashMap<String, Vec<Client>>,
    game_clients: Mutex<Vec<Client>>,
}

impl ServerState {
    fn new() -> Self {
        Self {
            registration_clients: DashMap::new(),
            game_clients: Mutex::new(Vec::new()),
        }
    }
}

async fn accept_connection(peer: SocketAddr, stream: TcpStream, state: Arc<ServerState>) {
    if let Err(e) = handle_connection(peer, stream, state).await {
        match e {
            Error::WebSocket(te) => match te {
                TungsteniteError::ConnectionClosed
                | TungsteniteError::Protocol(_)
                | TungsteniteError::Utf8 => (),
                err => error!("Error processing connection: {}", err),
            },
            e => error!("{:?}", e),
        }
    }
}

async fn handle_connection(
    _peer: SocketAddr,
    stream: TcpStream,
    state: Arc<ServerState>,
) -> Result<(), Error> {
    let ws_stream = accept_async(stream).await.map_err(Error::WebSocket)?;
    let (ws_sender, mut ws_receiver) = ws_stream.split();
    let ws_sender = Arc::new(Mutex::new(ws_sender));

    while let Some(msg) = ws_receiver.next().await {
        let msg: TungsteniteMessage = msg.map_err(Error::WebSocket)?;
        // All accepted messages are text
        if msg.is_text() {
            let text = msg.into_text().map_err(Error::WebSocket)?;

            info!("received message {}", text);

            let parsed_message: Message = serde_json::from_str(&text).map_err(Error::Serde)?;

            let message_result =
                handle_message(parsed_message, ws_sender.clone(), state.clone()).await;
            match message_result {
                Err(Error::Serde(e)) => error!("Serialization error: {}", e),
                Err(Error::WebSocket(e)) => error!("Websocket error: {}", e),
                Err(Error::MissingClient) => error!("Expected other client to have connected!"),
                Ok(()) => {}
            }
        }
    }

    Ok(())
}

async fn handle_message(
    message: Message,
    stream: Arc<Mutex<SplitSink<WebSocketStream<TcpStream>, TungsteniteMessage>>>,
    state: Arc<ServerState>,
) -> Result<(), Error> {
    let session_info = &message.session;

    match &message.content {
        MessageContent::Register => {
            let session_info = session_info.clone();
            let clients = &state.registration_clients;
            let mut session_clients = clients
                .entry(session_info.session_uuid)
                .or_insert_with(Vec::new);

            // Notify all connected clients that a new client joined
            message.send_to_all(session_clients.iter()).await?;

            session_clients.push(Client::new(session_info.client_uuid, stream));
        }
        MessageContent::SubmitDescription { .. } => {
            let clients = &state.registration_clients;
            let session_clients = clients
                .get(&session_info.session_uuid)
                .ok_or(Error::MissingClient)?;
            // Forward to all other clients
            message
                .send_to_all(
                    session_clients
                        .iter()
                        .filter(|c| c.uuid != session_info.client_uuid),
                )
                .await?;
        }
        MessageContent::IceCandidate { .. } => {
            let clients = &state.registration_clients;
            let session_clients = clients
                .get(&session_info.session_uuid)
                .ok_or(Error::MissingClient)?;
            // Forward to all other clients
            message
                .send_to_all(
                    session_clients
                        .iter()
                        .filter(|c| c.uuid != session_info.client_uuid),
                )
                .await?;
        }
    }

    Ok(())
}

pub async fn start() {
    let addr = "0.0.0.0:9002";
    let mut listener = TcpListener::bind(&addr).await.expect("Can't listen");
    info!("Listening on: {}", addr);

    let state = Arc::new(ServerState::new());

    while let Ok((stream, _)) = listener.accept().await {
        let peer = stream
            .peer_addr()
            .expect("connected streams should have a peer address");
        info!("Peer address: {}", peer);

        tokio::spawn(accept_connection(peer, stream, state.clone()));
    }
}
