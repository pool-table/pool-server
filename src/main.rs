mod client;
mod error;
mod message;
mod server;

pub use {client::Client, error::Error, message::Message, message::MessageContent};

use dotenv::dotenv;

#[tokio::main]
async fn main() {
    dotenv().ok();
    env_logger::init();

    server::start().await;
}
